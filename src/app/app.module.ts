import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { TemplateformComponent } from './templateform/templateform.component';
import { FormsModule } from '@angular/forms';

import {ReactiveFormsModule} from '@angular/forms';
import { RaectiveformComponent } from './raectiveform/raectiveform.component';
@NgModule({
  declarations: [
    AppComponent,
    TemplateformComponent,
    RaectiveformComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
