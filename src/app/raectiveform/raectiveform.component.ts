import { Component } from '@angular/core';
import {FormControl, FormGroup, FormBuilder, Validators} from '@angular/forms';

@Component({
  selector: 'app-raectiveform',
  templateUrl: './raectiveform.component.html',
  styleUrls: ['./raectiveform.component.css']
})
export class RaectiveformComponent {

  signUpForm :  FormGroup;
  FirstName : string="";
  LastName : string="";
  Email : string ="";
  Password : string ="";

  submitted : boolean = false;

  constructor(private fb : FormBuilder)
  {
this.signUpForm= fb.group({
   FirstName : new FormControl("", [Validators.required, Validators.minLength(10)]),
   LastName: new FormControl("", [Validators.required]),
   Email : new FormControl("", [Validators.required,Validators.email]),
   Password: new FormControl()
})
     
}
  Submit(regForm : any)
  {
    this.submitted = true;
    if(this.signUpForm.invalid) return;
    else
  console.log(JSON.stringify(regForm.value))
  }
}
