import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RaectiveformComponent } from './raectiveform.component';

describe('RaectiveformComponent', () => {
  let component: RaectiveformComponent;
  let fixture: ComponentFixture<RaectiveformComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [RaectiveformComponent]
    });
    fixture = TestBed.createComponent(RaectiveformComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
